import { createAction } from 'redux-starter-kit';

const SET_SORT_FIELD = 'SET_SORT_FIELD';
const SET_SORT_ORDER = 'SET_SORT_ORDER';

export const setSortField = createAction(SET_SORT_FIELD);
export const setSortOrder = createAction(SET_SORT_ORDER);

const initialState = {
  field: 'username',
  order: 'asc',
};

export default function sortingReducer(state = initialState, action) {
  switch(action.type) {
    case SET_SORT_FIELD:
      return {
        ...state,
        field: action.payload,
      };
    case SET_SORT_ORDER:
      return {
        ...state,
        order: action.payload,
      };
    default:
      return state;
  }
}
