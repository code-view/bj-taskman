import { createAction } from 'redux-starter-kit';
import { navigate } from '@reach/router';

import fakeAuth from 'utils/fakeAuth';

const LOGIN_START = 'LOGIN_START';
const LOGIN_DONE = 'LOGIN_DONE';
const LOGIN_FAIL = 'LOGIN_FAIL';

const LOGOUT = 'LOGOUT';

const RESET_AUTH_ERROR = 'RESET_AUTH_ERROR';

export const loginStart = createAction(LOGIN_START);
export const loginDone = createAction(LOGIN_DONE);
export const loginFail = createAction(LOGIN_FAIL);

export const logout = createAction(LOGOUT);

export const resetAuthError = createAction(RESET_AUTH_ERROR);

export const login = (login, password) => async (dispatch) => {
  dispatch(loginStart());
  try {
    await fakeAuth({ login, password });
    dispatch(loginDone());
    navigate('/');
  } catch (e) {
    dispatch(loginFail({ message: e.message }));
  }
};

const initialState = {
  isAdmin: false,
  isTrying: false,
  error: '',
};

export default function authReducer(state = initialState, action) {
  switch(action.type) {
    case LOGIN_START:
      return {
        ...state,
        isAdmin: false,
        isTrying: true,
        error: '',
      };
    case LOGIN_DONE:
      return {
        ...state,
        isAdmin: true,
        isTrying: false,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isTrying: false,
        error: action.payload.message,
      };
    case LOGOUT:
      return {
        ...state,
        isAdmin: false,
      };
    case RESET_AUTH_ERROR:
      return {
        ...state,
        error: '',
      };
    default:
      return state;
  }
}