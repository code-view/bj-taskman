import { combineReducers } from 'redux';

import auth from './auth';
import tasks from './tasks';
import sorting from './sorting';

export default combineReducers({
  auth,
  tasks,
  sorting,
});