import { configureStore, getDefaultMiddleware } from 'redux-starter-kit';
import { createLogger } from 'redux-logger';

import monitorReducersEnhancer from './storeEnhancers'; 
import rootReducer from './rootReducer';

// must be the last middleware in the chain
const logger = createLogger({
  duration: true,
  predicate: (getState, action) => {
    const hiddenTypes = [];
    return !hiddenTypes.some(type => type === action.type);
  },
});

const isDevelopment = process.env.NODE_ENV === 'development';

const middleware = [
  ...getDefaultMiddleware(),
  isDevelopment && logger,
].filter(Boolean);

export default function createReduxStore(preloadedState = {}) {
  const store = configureStore({
    reducer: rootReducer,
    middleware,
    preloadedState,
    devTools: isDevelopment,
    enhancers: isDevelopment
      ? [monitorReducersEnhancer]
      : [],
  });
  return store;
}
