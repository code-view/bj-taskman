import { createSelector } from 'redux-starter-kit';
import countMaxPages from 'utils/countMaxPages';

// ~~~~~~~~~~~~~~~~ tasks ~~~~~~~~~~~~~~~~~~~

export const getTasks = state => state.tasks.list;

export const getTasksLoading = state => state.tasks.loading;

export const getCurrentPageNumber = state => state.tasks.page;

export const getTasksTotalCount = state => state.tasks.totalCount;

export const getPagesTotalCount = createSelector(
  ['tasks.totalCount'],
  tasksTotalCount => countMaxPages(tasksTotalCount),
);

export const getTasksLoadError = state => state.tasks.loadError;

export const getCreateTaskError = state => state.tasks.createError;

export const getTaskUpdateError = state => state.tasks.updateError;

// ~~~~~~~~~~~~~~~~ sorting ~~~~~~~~~~~~~~~~~~~

export const getSortField = state => state.sorting.field;

export const getSortOrder = state => state.sorting.order;

// ~~~~~~~~~~~~~~~~ auth ~~~~~~~~~~~~~~~~~~~

export const getAdminFlag = state => state.auth.isAdmin;

export const getTryingToAuth = state => state.auth.isTrying;

export const getAuthError = state => state.auth.error;
