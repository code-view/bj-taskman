import { createAction } from 'redux-starter-kit';

import { BASE_API_URL, DEVELOPER, TASKS_PER_PAGE } from 'config';
import {
  getCurrentPageNumber, getSortField, getSortOrder,
} from 'state/selectors';
import countMaxPages from 'utils/countMaxPages';
import fetchData, { createSignature } from 'utils/fetchData';

const LOAD_TASKS_START = 'LOAD_TASKS_START';
const LOAD_TASKS_SKIP = 'LOAD_TASKS_SKIP';
const LOAD_TASKS_DONE = 'LOAD_TASKS_DONE';
const LOAD_TASKS_FAIL = 'LOAD_TASKS_FAIL';

const CREATE_TASK_START = 'CREATE_TASK_START';
const CREATE_TASK_DONE = 'CREATE_TASK_DONE';
const CREATE_TASK_FAIL = 'CREATE_TASK_FAIL';
const RESET_CREATE_TASK_ERROR = 'RESET_CREATE_TASK_ERROR';

const UPDATE_TASK_START = 'UPDATE_TASK_START';
const UPDATE_TASK_DONE = 'UPDATE_TASK_DONE';
const UPDATE_TASK_FAIL = 'UPDATE_TASK_FAIL';
const RESET_TASK_UPDATE_ERROR = 'RESET_TASK_UPDATE_ERROR';

const PAGINATION_PREV_PAGE = 'PAGINATION_PREV_PAGE';
const PAGINATION_NEXT_PAGE = 'PAGINATION_NEXT_PAGE';

export const loadTasksStart = createAction(LOAD_TASKS_START);
export const loadTasksSkip = createAction(LOAD_TASKS_SKIP);
export const loadTasksDone = createAction(LOAD_TASKS_DONE);
export const loadTasksFail = createAction(LOAD_TASKS_FAIL);

export const createTaskStart = createAction(CREATE_TASK_START);
export const createTaskDone = createAction(CREATE_TASK_DONE);
export const createTaskFail = createAction(CREATE_TASK_FAIL);
export const resetCreateTaskError = createAction(RESET_CREATE_TASK_ERROR);

export const updateTaskStart = createAction(UPDATE_TASK_START);
export const updateTaskDone = createAction(UPDATE_TASK_DONE);
export const updateTaskFail = createAction(UPDATE_TASK_FAIL);
export const resetTaskUpdateError = createAction(RESET_TASK_UPDATE_ERROR);

export const prevPage = createAction(PAGINATION_PREV_PAGE);
export const nextPage = createAction(PAGINATION_NEXT_PAGE);

const skipOnPageChange = (initialPage, dispatch, state) => {
  const pageAfter = getCurrentPageNumber(state);

  if (initialPage !== pageAfter) {
    dispatch(loadTasksSkip({ initialPage, pageAfter }));
    return true;
  }
  return false;
};

export const loadTasks = () => async (dispatch, getState) => {
  const state = getState();
  const page = getCurrentPageNumber(state);
  const sortField = getSortField(state);
  const sortDirection = getSortOrder(state);
  dispatch(loadTasksStart());
  const url = `${BASE_API_URL}/?developer=${DEVELOPER}&` +
    `sort_field=${sortField}&sort_direction=${sortDirection}&page=${page}`;
  try {
    const result = await fetchData(url, { method: 'GET' });
    if (skipOnPageChange(page, dispatch, getState())) {
      return null;
    }
    const { tasks, total_task_count } = result.message;
    dispatch(loadTasksDone({ tasks: tasks, totalCount: total_task_count }));
    return result;
  } catch (error) {
    if (skipOnPageChange(page, dispatch, getState()) === false) {
      dispatch(loadTasksFail({ message: error.message }));
    }
    return null;
  }
};

export const createTask = (task) => async (dispatch) => {
  const { username, email, description: text } = task;
  dispatch(createTaskStart());
  const url = `${BASE_API_URL}/create/?developer=${DEVELOPER}`;
  const formData = new FormData();
  formData.append('username', username);
  formData.append('email', email);
  formData.append('text', text);
  try {
    const result = await fetchData(url, { body: formData });
    dispatch(createTaskDone({ ...result.message }));
    return result;
  } catch (error) {
    dispatch(createTaskFail({ message: error.message }));
    return null;
  }
};

export const updateTask = ({ task, ...changes }) => (
  async (dispatch, getState) => {
    const url = `${BASE_API_URL}/edit/${task.id}/?developer=${DEVELOPER}`;
    const signature = createSignature(changes);
    const formData = new FormData();
    Object.keys(changes).forEach((field) => (
      formData.append(field, changes[field])
    ));
    formData.append('token', 'beejee');
    formData.append('signature', signature);
    dispatch(updateTaskStart({ taskId: task.id, ...changes }));
    try {
      const result = await fetchData(url, { body: formData });
      dispatch(updateTaskDone(result));
      return result;
    } catch (error) {
      dispatch(updateTaskFail({ task, message: error.message }));
      return null;
    }
  }
);

const initialState = {
  list: [],
  totalCount: null,
  loading: false,
  creating: false,
  updating: false,
  page: 1,
  loadError: '',
  createError: '',
  updateError: '',
};

export function listReducer(listState = [], action) {
  const { payload = {} } = action;

  switch (action.type) {
    case LOAD_TASKS_DONE:
      return payload.tasks;

    case LOAD_TASKS_FAIL:
      return [];

    case CREATE_TASK_DONE:
      return [
        payload,
        ...listState.slice(0, TASKS_PER_PAGE - 1),
      ];

    case UPDATE_TASK_START: {
      const { taskId, status } = payload;
      const updatedTaskList = listState.map(task => (
        task.id !== taskId
          ? task
          : {
            ...task,
            status: typeof status !== 'undefined' ? status : task.status,
            text: payload.text || task.text,
          }
      ));
      return updatedTaskList;
    }

    case UPDATE_TASK_FAIL:
      return listState.map(task => task.id === payload.task.id
        ? payload.task
        : task
      );
    
    default:
      return listState;
  }
}

export default function tasksReducer(state = initialState, action) {
  const { payload = {} } = action;

  switch (action.type) {
    case LOAD_TASKS_START:
      return { ...state, loading: true, loadError: '' };

    case LOAD_TASKS_DONE:
      return {
        ...state,
        loading: false,
        list: listReducer(state.list, action),
        totalCount: Number(payload.totalCount),
      };

    case LOAD_TASKS_FAIL:
      return {
        ...state,
        loading: false,
        list: listReducer(state.list, action),
        totalCount: null,
        loadError: payload.message,
      };

    case CREATE_TASK_START:
      return {
        ...state,
        creating: true,
        createError: '',
      };

    case CREATE_TASK_DONE:
      return {
        ...state,
        creating: false,
        list: listReducer(state.list, action),
      };

    case CREATE_TASK_FAIL:
      return {
        ...state,
        creating: false,
        createError: payload.message,
      };

    case RESET_CREATE_TASK_ERROR:
      return { ...state, createError: '' };

    case UPDATE_TASK_START:
      return {
        ...state,
        list: listReducer(state.list, action),
        updating: true,
      };

    case UPDATE_TASK_DONE:
      return {
        ...state,
        updating: false,
      };

    case UPDATE_TASK_FAIL:
      return {
        ...state,
        updateError: payload.task.email, // will be used in message for user
        updating: false,
        list: listReducer(state.list, action),
      };

    case RESET_TASK_UPDATE_ERROR:
      return { ...state, updateError: '' };

    case PAGINATION_PREV_PAGE:
      return state.page > 1 
        ? { ...state, page: state.page - 1 }
        : state;

    case PAGINATION_NEXT_PAGE:
      return state.page < countMaxPages(state.totalCount)
        ? { ...state, page: state.page + 1 }
        : state;

    default:
      return state;
  }
}