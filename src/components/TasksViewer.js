import React, { Component } from 'react';
import { connect } from 'react-redux';
import pT from 'prop-types';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import LoadingSpinner from 'components/LoadingSpinner';
import TaskCard from 'components/TaskCard';
import Notification from 'components/Notification';

import {
  loadTasks, prevPage, nextPage, updateTask, resetTaskUpdateError,
} from 'state/tasks';
import {
  getTasks, getPagesTotalCount, getCurrentPageNumber, getAdminFlag,
  getTasksLoading, getTasksTotalCount, getTasksLoadError, getTaskUpdateError,
} from 'state/selectors';

const styles = theme => ({
  noTasks: {
    paddingTop: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,
    display: 'flex',
    justifyContent: 'center',
  },
  reloadButton: {
    marginLeft: '16px',
  },
  paginationControls: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  paginationButtons: {
    marginLeft: '16px',
  },
});

class TasksViewer extends Component {
  componentDidMount() {
    this.props.loadTasks();
  }

  componentDidUpdate(prevProps) {
    const { currentPage } = this.props;

    if (currentPage !== prevProps.currentPage) {
      this.props.loadTasks();
    }
  }

  switchToPrevPage = () => this.props.prevPage()

  switchToNextPage = () => this.props.nextPage()

  render() {
    const {
      classes, tasks, isAdmin, loadingTasks, taskUpdateError,
      tasksTotalCount, tasksLoadError, currentPage, loadTasks,
      pagesTotalCount, updateTask, resetTaskUpdateError,
    } = this.props;

    const NoTasksBlock = (tasksTotalCount === null || tasksTotalCount < 1) && (
      <div className={classes.noTasks}>
        <Typography color="textSecondary" variant="headline">
          {tasksLoadError ? 'Failed to load tasks' : 'No tasks'}
        </Typography>
        {tasksLoadError && (
          <Button
            className={classes.reloadButton}
            variant="contained"
            onClick={() => loadTasks()}
          >
            Load Again
          </Button>
        )}
      </div>
    );

    return (
      <Grid container direction="column" spacing={16}>
        <Grid item xs={12}>
          <div className={classes.paginationControls}>
            <Typography variant="subtitle2" >
              {`Page ${currentPage} of ${pagesTotalCount}`}
            </Typography>
            <div className={classes.paginationButtons}>
              <IconButton
                disabled={currentPage === 1}
                size="small"
                onClick={this.switchToPrevPage}
              >
                <Icon>navigate_before</Icon>
              </IconButton>
              <IconButton
                disabled={currentPage === pagesTotalCount}
                size="small"
                onClick={this.switchToNextPage}
              >
                <Icon>navigate_next</Icon>
              </IconButton>
            </div>
          </div>
        </Grid>

        {loadingTasks
          ? <LoadingSpinner show={loadingTasks} />
          : NoTasksBlock
        }

        {!loadingTasks && tasks.map((task) => (
          <Grid item key={task.id} xs={12}>
            <TaskCard task={task} updateTask={updateTask} isAdmin={isAdmin} />
          </Grid>
        ))}

        <Notification
          message={taskUpdateError
            ? `Failed to update task with email: ${taskUpdateError}`
            : ''}
          position={{ vertical: 'bottom' }}
          onClose={resetTaskUpdateError}
        />
      </Grid>
    );
  }
}

TasksViewer.propTypes = {
  classes: pT.object.isRequired,
  isAdmin: pT.bool.isRequired,
  tasks: pT.arrayOf(pT.object).isRequired,
  tasksTotalCount: pT.number,
  tasksLoadError: pT.string.isRequired,
  taskUpdateError: pT.string.isRequired,
  pagesTotalCount: pT.number.isRequired,
  currentPage: pT.number.isRequired,
  loadTasks: pT.func.isRequired,
  updateTask: pT.func.isRequired,
  resetTaskUpdateError: pT.func.isRequired,
};

const mapStateToProps = state => ({
  isAdmin: getAdminFlag(state),
  tasks: getTasks(state),
  tasksTotalCount: getTasksTotalCount(state),
  tasksLoadError: getTasksLoadError(state),
  taskUpdateError: getTaskUpdateError(state),
  loadingTasks: getTasksLoading(state),
  pagesTotalCount: getPagesTotalCount(state),
  currentPage: getCurrentPageNumber(state),
});

export default connect(mapStateToProps, {
  loadTasks,
  updateTask,
  prevPage,
  nextPage,
  resetTaskUpdateError,
})(withStyles(styles)(TasksViewer));
