import React from 'react';
import pT from 'prop-types';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Snackbar from '@material-ui/core/Snackbar';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  close: {
    padding: theme.spacing.unit / 2,
  },
});

function Notification({ classes, message, onClose, position }) {
  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right', ...position }}
      open={Boolean(message)}
      ContentProps={{
        'aria-describedby': 'message-id',
      }}
      message={<span id="message-id">{message}</span>}
      action={(
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={() => onClose()}
        >
          <Icon>close</Icon>
        </IconButton>
      )}
    />
  );
}

Notification.propTypes = {
  classes: pT.object.isRequired,
  message: pT.string,
  onClose: pT.func.isRequired,
  position: pT.object,
};

export default withStyles(styles)(Notification);