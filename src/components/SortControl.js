import React, { Component } from 'react';
import { connect } from 'react-redux';
import pT from 'prop-types';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';

import { loadTasks } from 'state/tasks';
import { setSortField, setSortOrder } from 'state/sorting';
import { getSortField, getSortOrder } from 'state/selectors';

const styles = theme => ({
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: 0,
    paddingBottom: 0,
    marginBottom: '20px',
  },
  sortTitle: {
    marginRight: 'auto',
    marginTop: '10px',
    marginBottom: '10px',
  },
  form: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    marginLeft: '14px',
    minWidth: 160,
    marginRight: '-2px',
  },
});

class SortControl extends Component {
  handleSelect = (event) => {
    const { name, value } = event.target;
    const {
      sortField, sortOrder, setSortField, setSortOrder, loadTasks,
    } = this.props;

    if (name === 'sort-field' && sortField !== value) {
      setSortField(value);
      loadTasks();
    } else if (name === 'sort-order' && sortOrder !== value) {
      setSortOrder(value);
      loadTasks();
    }
  }

  render() {
    const { classes, sortField, sortOrder } = this.props;

    return (
      <Paper className={classes.paper}>
        <form className={classes.form} noValidate autoComplete="off">
          <Typography className={classes.sortTitle} variant="subtitle1">
            Select sort:
          </Typography>
          <div>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-sort-field">Sorting field</InputLabel>
              <Select
                value={sortField}
                onChange={this.handleSelect}
                input={<FilledInput name="sort-field" id="filled-sort-field" />}
              >
                <MenuItem value="username">Name</MenuItem>
                <MenuItem value="email">Email</MenuItem>
                <MenuItem value="status">Status</MenuItem>
              </Select>
            </FormControl>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel htmlFor="filled-sort-order">Sorting order</InputLabel>
              <Select
                value={sortOrder}
                onChange={this.handleSelect}
                input={<FilledInput name="sort-order" id="filled-sort-order" />}
              >
                <MenuItem value="asc">Ascending</MenuItem>
                <MenuItem value="desc">Descending</MenuItem>
              </Select>
            </FormControl>
          </div>
        </form>
      </Paper>
    );
  }
}

SortControl.propTypes = {
  classes: pT.object.isRequired,
  setSortField: pT.func.isRequired,
  setSortOrder: pT.func.isRequired,
  loadTasks: pT.func.isRequired,
  sortField: pT.string.isRequired,
  sortOrder: pT.string.isRequired,
};

const mapStateToProps = (state) => ({
  sortField: getSortField(state),
  sortOrder: getSortOrder(state),
});

export default connect(mapStateToProps, {
  loadTasks,
  setSortField,
  setSortOrder,
})(withStyles(styles)(SortControl));