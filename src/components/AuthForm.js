import React, { useState } from 'react';
import pT from 'prop-types';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  buttonWrapper: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 6,
    position: 'relative',
  },
  buttonProgress: {
    color: '#ccc',
    position: 'absolute',
    top: '50%',
    left: '34%',
    marginTop: -12,
    marginLeft: -12,
  },
});

const AuthForm = (props) => {
  const { classes, tryingToAuth, onLogin } = props;

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = (event) => {
    event.preventDefault();
    onLogin(login, password);
    setLogin('');
    setPassword('');
  };

  return (
    <form className={classes.form} onSubmit={handleLogin}>
      <FormControl margin="normal" required fullWidth>
        <InputLabel htmlFor="login">Login</InputLabel>
        <Input
          id="login"
          name="login"
          autoComplete="username"
          autoFocus
          required
          value={login}
          onChange={(event) => setLogin(event.target.value)}
        />
      </FormControl>
      <FormControl margin="normal" required fullWidth>
        <InputLabel htmlFor="password">Password</InputLabel>
        <Input
          name="password"
          type="password"
          id="password"
          autoComplete="current-password"
          required
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        />
      </FormControl>
      <div className={classes.buttonWrapper}>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
        >
          Sign in
        </Button>
        {tryingToAuth && (
          <CircularProgress
            className={classes.buttonProgress}
            size={24}
            variant="indeterminate"
            disableShrink
            thickness={4}
          />
        )}
      </div>
    </form>
  );
};

AuthForm.propTypes = {
  classes: pT.object.isRequired,
  tryingToAuth: pT.bool.isRequired,
  onLogin: pT.func.isRequired,
};

export default withStyles(styles)(AuthForm);
