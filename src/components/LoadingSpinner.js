import React from 'react';
import pT from 'prop-types';

import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  progress: {
    margin: theme.spacing.unit * 2,
    animationDuration: '1000ms',
  },
});

function LoadingSpinner({ classes, show = false }) {
  if (!show) return null;

  return (
    <div className={classes.container}>
      <Fade
        in={show}
        style={{
          transitionDelay: show ? '800ms' : '0ms',
        }}
        unmountOnExit
      >
        <CircularProgress
          className={classes.progress}
          thickness={4}
        />
      </Fade>
    </div>
  );
}

LoadingSpinner.propTypes = {
  show: pT.bool,
  classes: pT.object.isRequired,
};

export default withStyles(styles)(LoadingSpinner);
