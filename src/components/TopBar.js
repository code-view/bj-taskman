import React from 'react';
import { connect } from 'react-redux';
import pT from 'prop-types';
import { navigate, Link as RouterLink } from '@reach/router';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';

import { logout } from 'state/auth';
import { getAdminFlag } from 'state/selectors';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  homeLink: {
    '&:hover': {
      textDecoration: 'none',
    },
  },
});

function TopBar({ classes, isAdmin, showAuthButton, logout }) {
  let authButton = null;

  if (showAuthButton) {
    authButton = isAdmin
      ? (
        <Button
          color="primary"
          variant="outlined"
          onClick={() => logout()}>
          Logout
        </Button>
      )
      : (
        <Button
          color="primary"
          variant="outlined"
          onClick={() => navigate('/login')}>
          Login
        </Button>
      );
  }

  return (
    <AppBar position="static" color="default" className={classes.appBar}>
      <Toolbar variant="dense" className={classes.toolbar}>
        <Typography variant="h6" color="inherit" noWrap>
          <Link to="/" component={RouterLink} className={classes.homeLink}>
            Task Manager
          </Link>
        </Typography>
        {authButton} 
      </Toolbar>
    </AppBar>
  );
}

TopBar.defaultProps = {
  showAuthButton: true,
};

TopBar.propTypes = {
  classes: pT.object.isRequired,
  isAdmin: pT.bool.isRequired,
  logout: pT.func.isRequired,
  showAuthButton: pT.bool,
};

const mapStateToProps = (state) => ({
  isAdmin: getAdminFlag(state),
});

export default connect(mapStateToProps, {
  logout,
})(withStyles(styles)(TopBar));