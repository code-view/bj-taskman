import React, { useState, memo } from 'react';
import pT from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  cardHeader: {
    backgroundColor: theme.palette.grey[200],
    textAlign: 'center',
    padding: '2px',
  },
  cardHeaderTitle: {
    paddingRight: '30px',
    fontSize: '16px',
  },
  cardHeaderAction: {
    marginTop: '4px',
    marginRight: 0,
  },
  moreButton: {
    padding: '6px',
  },
  cardContent: {
    padding: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3,
    },
  },
  cardActions: {
    paddingLeft: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit / 2,
    paddingTop: 0,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: theme.spacing.unit * 3,
    },
  },
  taskTextField: {
    margin: 0,
  },
  checkboxLabel: {
    lineHeight: '1.5em',
  },
});

function TaskCard(props) {
  const { classes, isAdmin, task, updateTask } = props;

  const [isEditing, setEditMode] = useState(false);
  const [status, setTaskStatus] = useState(task.status);
  const [taskText, setTaskText] = useState(task.text);

  const handleEdit = (event) => {
    if (taskText.trim().length < 1) {
      return;
    }
    if (isEditing && taskText !== task.text) {
      updateTask({
        task,
        text: taskText,
      });
    }
    setEditMode(isEditing => !isEditing);
  };

  const changeStatus = (event) => {
    const newStatus = event.target.checked ? 10 : 0;
    setTaskStatus(newStatus);
    updateTask({
      task,
      status: newStatus,
    });
  };

  return (
    <Card className={classes.card}>
      <CardHeader
        title={(
          <Typography
            className={classes.cardHeaderTitle}
            inline
            component="h3"
            variant="overline"
            color="textPrimary"
          >
            {task.username}
          </Typography>
        )}
        disableTypography
        subheader={(
          <Typography
            inline
            component="h5"
            variant="subtitle1"
            color="textSecondary"
          >
            {task.email}
          </Typography>
        )}
        action={isAdmin && (
          <IconButton
            className={classes.moreButton}
            size="small"
            onClick={handleEdit}
          >
            <Icon color="action">{isEditing ? 'save' : 'edit'}</Icon>
          </IconButton>
        )}
        classes={{ action: classes.cardHeaderAction }}
        className={classes.cardHeader}
      />
      <CardContent className={classes.cardContent}>
        {isEditing && isAdmin
          ? (
            <TextField
              id="outlined-task-text-input"
              required
              error={taskText.trim().length < 1}
              className={classes.taskTextField}
              type="text"
              name="taskText"
              margin="dense"
              variant="outlined"
              fullWidth
              multiline
              value={taskText}
              onChange={(event) => setTaskText(event.target.value)}
            />
          )
          :(
          <Typography variant="subtitle1">
            {task.text}
          </Typography>
        )}
      </CardContent>
      <CardActions className={classes.cardActions}>
        <FormControlLabel
          classes={{ label: classes.checkboxLabel }}
          control={(
            <Checkbox
              checked={status === 10}
              disabled={isAdmin === false}
              value="remember"
              color="primary"
              onChange={changeStatus}
            />
          )}
          label="Completed"
        />
      </CardActions>
    </Card>
  );
}

TaskCard.propTypes = {
  classes: pT.object.isRequired,
  isAdmin: pT.bool.isRequired,
  task: pT.object.isRequired,
  updateTask: pT.func.isRequired,
};

export default withStyles(styles)(memo(TaskCard));