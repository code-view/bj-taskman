import React, { Component } from 'react';
import { connect } from 'react-redux';
import pT from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

import Notification from 'components/Notification';

import { createTask, resetCreateTaskError } from 'state/tasks';
import { getCreateTaskError } from 'state/selectors';
import { isEmail } from 'utils';

const styles = theme => ({
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    marginBottom: '20px',
  },
  textField: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  description: {
    marginBottom: theme.spacing.unit,
  },
  addButton: {
    margin: '8px',
    marginLeft: 'auto',
    marginRight: 0,
  },
  buttonContainer: {
    display: 'flex',
  },
});

class AddTaskForm extends Component {
  state = {
    username: '',
    email: '',
    description: '',
    checksFailed: false,
    wrongEmail: false,
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;
    this.setState(({ wrongEmail }) => ({
      [name]: value,
      // remove error state on email input if value was changed
      wrongEmail: (wrongEmail && name === 'email') ? false : wrongEmail,
    }));
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { username, email, description } = this.state;
    const wrongEmail = isEmail(email) === false;
    const hasEmptyFields = [
      username.trim(), email.trim(), description.trim(),
    ].includes('');

    if (hasEmptyFields || wrongEmail) {
      this.setState({
        checksFailed: hasEmptyFields || wrongEmail,
        wrongEmail,
      });
      return;
    }
    this.props.createTask({ username, email, description });
    this.setState({
      username: '',
      email: '',
      description: '',
      checksFailed: false,
      wrongEmail: false,
    });
  }

  render() {
    const { classes, createTaskError, resetCreateTaskError } = this.props;
    const {
      username, email, description, checksFailed, wrongEmail,
    } = this.state;

    return (
      <Paper className={classes.paper}>
        <form noValidate onSubmit={this.handleSubmit}>
          <Grid container>
            <Grid item xs={10}>
              <TextField
                id="outlined-username-input"
                label="Name"
                required
                error={checksFailed && username.trim().length < 1}
                className={classes.textField}
                name="username"
                margin="dense"
                variant="outlined"
                value={username}
                onChange={this.handleInputChange}
              />
              <TextField
                id="outlined-email-input"
                label="Email"
                required
                error={checksFailed && (email.trim().length < 1 || wrongEmail)}
                className={classes.textField}
                type="email"
                name="email"
                autoComplete="email"
                margin="dense"
                variant="outlined"
                value={email}
                onChange={this.handleInputChange}
              />
            </Grid>
            <Grid item xs={2}>
              <div className={classes.buttonContainer}>
                <Button
                  className={classes.addButton}
                  color="primary"
                  variant="contained"
                  size="large"
                  type="submit"
                >
                  Add
                </Button>
              </div>
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="outlined-description-input"
                label="Description"
                required
                error={checksFailed && description.trim().length < 1}
                className={`${classes.description} ${classes.textField}`}
                type="text"
                name="description"
                variant="outlined"
                fullWidth
                multiline
                placeholder="Write task description here"
                rows="1"
                rowsMax="2"
                value={description}
                onChange={this.handleInputChange}
              />
            </Grid>
          </Grid>
        </form>
        <Notification
          message={createTaskError
            ? `Failed to create task. Reason: ${createTaskError}`
            : ''
          }
          position={{ vertical: 'bottom' }}
          onClose={resetCreateTaskError}
        />
      </Paper>
    );
  }
}

AddTaskForm.propTypes = {
  classes: pT.object.isRequired,
  createTaskError: pT.string.isRequired,
  createTask: pT.func.isRequired,
  resetCreateTaskError: pT.func.isRequired,
};

export default connect(
  state => ({ createTaskError: getCreateTaskError(state) }),
  {
    createTask,
    resetCreateTaskError,
  },
)(withStyles(styles)(AddTaskForm));
