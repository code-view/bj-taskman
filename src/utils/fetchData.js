import md5 from 'md5';
import queryString from 'query-string';

export const getQueryString = (params = {}, sort = false) => (
  queryString.stringify(params, { sort })
);

const order = ['status', 'text', 'token'];

const sortFunc = (curr, next) => order.indexOf(curr) - order.indexOf(next);

export const createSignature = (params = {}) => {
  const paramsString = getQueryString({ token: 'beejee', ...params }, sortFunc);
  return md5(paramsString);
};

async function fetchData(url, requestObj = {}) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      "Accept": "application/json",
    },
    cors: true,
    ...requestObj,
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const { status, message } = await response.json();

  if (status !== 'ok') {
    throw new Error(`${JSON.stringify(message, null, 2)}`);
  }
  return { status, message };
}

export default fetchData;
