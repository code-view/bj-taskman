import { TASKS_PER_PAGE } from 'config';

const countMaxPages = (itemsTotalCount) => (
  !itemsTotalCount ? 1 : Math.ceil(itemsTotalCount / TASKS_PER_PAGE)
);

export default countMaxPages;
