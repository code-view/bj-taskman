export const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

export const isEmail = (email) => (
  /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
);