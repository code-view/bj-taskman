import { delay } from 'utils';

export default async function fakeAuth(data) {
  await delay(2000);

  const { login, password } = data;

  if (login !== 'admin' || password !== '123') {
    throw new Error('Incorrect email or password');
  }
  return { status: 'ok' };
}