import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'isomorphic-fetch';
import { Provider } from 'react-redux';
// some sort of css reset, like normalize.css
import CssBaseline from '@material-ui/core/CssBaseline';

import App from './App';
import createReduxStore from './state/createReduxStore';

const store = createReduxStore();

ReactDOM.render((
  <Fragment>
    <CssBaseline />
    <Provider store={store}>
      <App />
    </Provider>
  </Fragment>
), document.getElementById('root'));
