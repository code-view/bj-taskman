import React, { Fragment, memo } from 'react';
import pT from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import TopBar from 'components/TopBar';
import AddTaskForm from 'components/AddTaskForm';
import TasksViewer from 'components/TasksViewer';
import SortControl from 'components/SortControl';

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginTop: '16px',
    [theme.breakpoints.up(900 + theme.spacing.unit * 3 * 2)]: {
      width: 900,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
});

function Home(props) {
  const { classes } = props;

  return (
    <Fragment>
      <TopBar />
      <main className={classes.layout}>
        <AddTaskForm />
        <SortControl />
        <TasksViewer />
      </main>
    </Fragment>
  );
}

Home.propTypes = {
  classes: pT.object.isRequired,
};

export default withStyles(styles)(memo(Home));