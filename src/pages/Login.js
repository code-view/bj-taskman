import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import pT from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import TopBar from 'components/TopBar';
import AuthForm from 'components/AuthForm';
import Notification from 'components/Notification';

import { login, resetAuthError } from 'state/auth';
import { getAdminFlag, getTryingToAuth, getAuthError } from 'state/selectors';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 4,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
});

function Login(props) {
  const {
    classes, isAdmin, tryingToAuth, navigate,
    dispatchLogin, authError, resetAuthError,
  } = props;

  if (isAdmin) {
    navigate('/');
    return null;
  }

  return (
    <Fragment>
      <TopBar showAuthButton={false} />
      <main className={classes.main}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Icon>lock</Icon>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <AuthForm
            tryingToAuth={tryingToAuth}
            onLogin={dispatchLogin}
          />
        </Paper>
        <Notification message={authError} onClose={resetAuthError} />
      </main>
    </Fragment>
  );
}

Login.propTypes = {
  classes: pT.object.isRequired,
  isAdmin: pT.bool.isRequired,
  tryingToAuth: pT.bool.isRequired,
  navigate: pT.func.isRequired,
  dispatchLogin: pT.func.isRequired,
  resetAuthError: pT.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAdmin: getAdminFlag(state),
  tryingToAuth: getTryingToAuth(state),
  authError: getAuthError(state),
});

export default connect(mapStateToProps, {
  dispatchLogin: login,
  resetAuthError,
})(withStyles(styles)(Login));
